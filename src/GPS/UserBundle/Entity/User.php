<?php

namespace GPS\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class User extends BaseUser
{
    const ROLE_ADMIN    = 'ROLE_ADMIN';
    const ROLE_USER     = 'ROLE_USER';

    public static $ROLES = array(
        self::ROLE_ADMIN    => 'Administrador',
        self::ROLE_USER     => 'Usuario'
    );


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ruc", type="string", length=15)
     */
    protected $ruc = "0";

    /**
     * @var string
     *
     * @ORM\Column(name="phones", type="string", length=255)
     */
    protected $phones = "0";

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    protected $options = array();

    /**
     * @ORM\ManyToMany(targetEntity="GPS\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ruc
     *
     * @param string $ruc
     * @return User
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;

        return $this;
    }

    /**
     * Get ruc
     *
     * @return string
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set phones
     *
     * @param string $phones
     * @return User
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return string
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set options
     *
     * @param array $options
     * @return User
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function getRoleNames()
    {
        $roles = array();

        foreach ($this->getRoles() as $role) {
            $roles[] = str_replace('_', ' ', substr($role, 5));
        }

        return $roles;
    }
}
