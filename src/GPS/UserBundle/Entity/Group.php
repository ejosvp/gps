<?php

namespace GPS\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use GPS\GPSBundle\Entity\Vehicle;

/**
 * Group
 *
 * @ORM\Table(name="gps_group")
 * @ORM\Entity
 */
class Group extends BaseGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Group", mappedBy="parent")
     */
    protected $children;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="children")
     **/
    protected $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GPS\GPSBundle\Entity\Vehicle", mappedBy="group")
     */
    protected $vehicles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\ManyToMany(targetEntity="GPS\UserBundle\Entity\User", mappedBy="groups")
     *
     */
    protected $users;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
        $this->enabled = true;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param ArrayCollection $children
     * @return $this
     */
    public function setChildren(ArrayCollection $children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Group $child
     * @return $this
     */
    public function addChild(Group $child)
    {
        if (!$this->getChildren()->contains($child))
            $this->getChildren()->add($child);

        return $this;
    }

    /**
     * @param Group $child
     * @return $this
     */
    public function removeChild(Group $child)
    {
        if (!$this->getChildren()->contains($child))
            $this->getChildren()->removeElement($child);

        return $this;
    }

    /**
     * @param $parent
     * @return $this
     */
    public function setParent(Group $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Group
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ArrayCollection $vehicles
     * @return $this
     */
    public function setVehicles(ArrayCollection $vehicles)
    {
        $this->vehicles = $vehicles;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * @param Vehicle $vehicle
     * @return $this
     */
    public function addVehicle(Vehicle $vehicle)
    {
        if (!$this->getVehicles()->contains($vehicle))
            $this->getVehicles()->add($vehicle);

        return $this;
    }

    /**
     * @param Vehicle $vehicle
     * @return $this
     */
    public function removeVehicle(Vehicle $vehicle)
    {
        if (!$this->getVehicles()->contains($vehicle))
            $this->getVehicles()->removeElement($vehicle);

        return $this;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function isEnabled()
    {
        return (bool) $this->enabled;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }



}