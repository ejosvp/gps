<?php

namespace GPS\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use GPS\UserBundle\Entity\User;

class UserType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('enabled')
            ->add('roles')
            ->add('ruc')
            ->add('phones')
            ->add('groups')
            ->add('options');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'gps_userbundle_usertype';
    }
}
