<?php

namespace GPS\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\UserBundle\Entity\User;
use GPS\UserBundle\Form\UserType;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="user")
     * @Template()
     */
    public function indexAction()
    {
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        return array(
            'users' => $users,
        );
    }

    /**
     * Finds and displays a User
     *
     * @Route("/{username}/show", name="user_show")
     * @Template()
     */
    public function showAction($username)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }

        return array(
            'user'      => $user,
        );
    }

    /**
     * Displays a form to create a new User
     *
     * @Route("/new", name="user_new")
     * @Template()
     */
    public function newAction()
    {
        $user = new User();
        $form   = $this->createForm(new UserType(get_class($user)), $user);

        return array(
            'user' => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new User
     *
     * @Route("/create", name="user_create")
     * @Method("POST")
     * @Template("GPSUserBundle:User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $user  = new User();
        $form = $this->createForm(new UserType(get_class($user)), $user);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('user'));
        }

        return array(
            'user' => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('GPSUserBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }

        $form = $this->createForm(new UserType(get_class($user)), $user);

        return array(
            'user'      => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Edits an existing User
     *
     * @Route("/{id}/update", name="user_update")
     * @Method("POST")
     * @Template("GPSUserBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('GPSUserBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }

        $form = $this->createForm(new UserType(get_class($user)), $user);
        $form->bind($request);

        if ($form->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('user'));
        }

        return array(
            'user'      => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Enable/Disable User
     *
     * @Route("/{username}/activate", name="user_activate")
     */
    public function activateAction(Request $request, $username)
    {
        /** @var $userManager \FOS\UserBundle\Entity\UserManager */
        $userManager = $this->get('fos_user.user_manager');

        /** @var $user User */
        $user = $userManager->findUserByUsername($username);

        if (!$user) {
            throw new \InvalidArgumentException(sprintf('User identified by "%s" username does not exist.', $username));
        }
        $user->setEnabled(!$user->isEnabled());
        $userManager->updateUser($user);

        return $this->redirect($this->generateUrl('user'));
    }
}
