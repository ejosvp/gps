<?php

namespace GPS\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\UserBundle\Entity\Group;

/**
 * Group controller.
 *
 * @Route("/group")
 */
class GroupController extends Controller
{

    /**
     * Enable/Disable User
     *
     * @Route("/{name}/activate", name="group_activate")
     */
    public function activateAction(Request $request, $name)
    {
        /** @var $groupManager \FOS\UserBundle\Entity\GroupManager */
        $groupManager = $this->get('fos_user.group_manager');

        /** @var $group Group */
        $group = $groupManager->findGroupByName($name);

        if (!$group) {
            throw new \InvalidArgumentException(sprintf('Group identified by "%s" name does not exist.', $name));
        }
        $group->setEnabled(!$group->isEnabled());
        $group->setRoles(array());
        $groupManager->updateGroup($group);

        return $this->redirect($this->generateUrl('fos_user_group_list'));
    }
}
