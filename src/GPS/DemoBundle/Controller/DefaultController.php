<?php

namespace GPS\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use GPS\GPSBundle\Entity\Plot;

/**
 * @Route("/demo")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="demo")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/items", name="demo_items")
     */
    public function itemsAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        /** @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        $items = $em->createQuery('
            SELECT d.longitude, d.latitude, v.id, g.icon, d.timestamp, d.speedOverGround
            FROM GPS\GPSBundle\Entity\Plot d
            INNER JOIN d.trackId v
            INNER JOIN v.group g
            INNER JOIN g.users u
            WHERE u.id = :user_id
            GROUP BY d.trackId
            ORDER BY d.datestamp, d.timestamp DESC
        ')
            ->setParameter('user_id', $user)
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $data = array();
        foreach($items as $item) {
            $data[] = array(
                'latLng' => array($item['latitude'], $item['longitude']),
                'id' => $item['id'],
                'timestamp' => $item['timestamp']->format('H:i:s'),
                'data' => 'Tracker: ' . $item['id'] . '<br/>Speed: ' . $item['speedOverGround'],
                'options' => array('icon'=> $item['icon']),
            );
        }

        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, array(new JsonEncoder()));

        $response = new Response();
//        $response->setContent($serializer->serialize($data, 'json'));
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/tracking/{plate}/{beginDate}/{beginTime}/{endDate}/{endTime}", name="demo_tracking", defaults={"beginDate" = 0, "beginTime" = 0, "endDate" = 0, "endTime" = 0})
     */
    public function trackingAction($plate, $beginDate, $beginTime, $endDate, $endTime)
    {
        /** @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();
        $items = ($beginDate == 0)
            ? $em->getRepository('GPSBundle:Plot')->findLastByPlate($plate)
            : $em->getRepository('GPSBundle:Plot')->findByPlateAndDate($plate, $beginDate, $beginTime, $endDate, $endTime);
        $data = array();
        foreach($items as $k => $item) {
            $data[] = array(
                'latLng' => array($item['latitude'], $item['longitude']),
                'id' => $item['id'] . "-" . $k,
                'speed' => $item['speedOverGround'],
                'datestamp' => $item['datestamp']->format('d/m/y'),
                'timestamp' => $item['timestamp']->format('H:i:s'),
                'data' => 'Tracker: ' . $item['id'] . '<br/>Speed: ' . $item['speedOverGround'],
                'options' => array('icon'=> $item['icon']),
            );
        }

        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, array(new JsonEncoder()));

        $response = new Response();
//        $response->setContent($serializer->serialize($data, 'json'));
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
