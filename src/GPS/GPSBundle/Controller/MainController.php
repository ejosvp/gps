<?php

namespace GPS\GPSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\GPSBundle\Form\FilterType;
use Doctrine\Common\Util\Debug;

/**
 * Main controller.
 *
 * @Route("/")
 */
class MainController extends Controller
{
    /**
     * Show Dashboard
     *
     * @Route("/", name="dashboard")
     * @Template()
     */
    public function dashboardAction()
    {
        return $this->redirect($this->generateUrl('dashboard_tracking'));
    }

    /**
     * Show tracking
     *
     * @Route("/track/{plate}/{beginDate}/{beginTime}/{endDate}/{endTime}", name="dashboard_tracking", defaults={"plate" = "nothing", "beginDate" = 0, "beginTime" = 0, "endDate" = 0, "endTime" = 0})
     * @Template("GPSBundle:Main:dashboard.html.twig")
     */
    public function trackingAction($plate, $beginDate, $beginTime, $endDate, $endTime)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $vehicles = $em->getRepository('GPSBundle:Vehicle')->findByUser($user);

        //Debug::dump($vehicles[0]);die();

        $router = $this->get('router');
        $route = ($plate == "nothing")
            ? $router->generate('demo_items')
            : $router->generate('demo_tracking', array('plate' => $plate,'beginDate' => $beginDate,'beginTime' => $beginTime,'endDate'   => $endDate,'endTime'   => $endTime));
        return array(
            'route' => $route,
            'vehicles' => $vehicles,
            'filter_form' => $this->createForm(new FilterType())->createView(),
        );
    }

    /**
     * Filter
     *
     * @Route("/filter", name="dashboard_filter")
     * @Template()
     */
    public function filterAction()
    {
        $request = $this->getRequest();

        $data = array();

        $form = $this->createForm(new FilterType(), $data);

        if('POST' === $request->getMethod()){
            $form->bindRequest($request);

            if($form->isValid()){
                $data = $form->getData();

                return $this->redirect($this->generateUrl(
                    'dashboard_tracking',
                    array(
                        'plate'   => $data['content'],
                        'beginDate'   => $data['beginDate']->format('Y-m-d'),
                        'beginTime'   => $data['beginTime']->format('H:i:s'),
                        'endDate'   => $data['endDate']->format('Y-m-d'),
                        'endTime'   => $data['endTime']->format('H:i:s'),
                    )
                ));
            }
            return $this->redirect($this->generateUrl('dashboard'));
        }
        return $this->redirect($this->generateUrl('dashboard'));
    }

}
