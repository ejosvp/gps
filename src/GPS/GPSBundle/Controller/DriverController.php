<?php

namespace GPS\GPSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\GPSBundle\Entity\Driver;
use GPS\GPSBundle\Form\DriverType;

/**
 * Driver controller.
 *
 * @Route("/driver")
 */
class DriverController extends Controller
{
    /**
     * Lists all Driver entities.
     *
     * @Route("/", name="driver")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GPSBundle:Driver')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Driver entity.
     *
     * @Route("/{id}/show", name="driver_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:Driver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Driver entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Driver entity.
     *
     * @Route("/new", name="driver_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Driver();
        $form   = $this->createForm(new DriverType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Driver entity.
     *
     * @Route("/create", name="driver_create")
     * @Method("POST")
     * @Template("GPSBundle:Driver:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Driver();
        $form = $this->createForm(new DriverType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setStatus(0);
            $entity->upload();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('driver_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Driver entity.
     *
     * @Route("/{id}/edit", name="driver_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:Driver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Driver entity.');
        }

        $editForm = $this->createForm(new DriverType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Driver entity.
     *
     * @Route("/{id}/update", name="driver_update")
     * @Method("POST")
     * @Template("GPSBundle:Driver:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:Driver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Driver entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new DriverType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $entity->upload();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('driver_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Driver entity.
     *
     * @Route("/{id}/delete", name="driver_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GPSBundle:Driver')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Driver entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('driver'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**                                                       activat
     * Finds and displays a Driver entity.
     *
     * @Route("/{id}/activate", name="driver_activate")
     */
    public function activateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:Driver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Driver entity.');
        }

        $entity->setStatus(!$entity->isEnabled());

        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('driver'));
    }
}
