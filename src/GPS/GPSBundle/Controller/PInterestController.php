<?php

namespace GPS\GPSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\GPSBundle\Entity\PInterest;
use GPS\GPSBundle\Form\PInterestType;

/**
 * PInterest controller.
 *
 * @Route("/geo/pinterest")
 */
class PInterestController extends Controller
{
    /**
     * Lists all PInterest entities.
     *
     * @Route("/", name="geo_pinterest")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GPSBundle:PInterest')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a PInterest entity.
     *
     * @Route("/{id}/show", name="geo_pinterest_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:PInterest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PInterest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new PInterest entity.
     *
     * @Route("/new", name="geo_pinterest_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new PInterest();
        $form   = $this->createForm(new PInterestType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new PInterest entity.
     *
     * @Route("/create", name="geo_pinterest_create")
     * @Method("POST")
     * @Template("GPSBundle:PInterest:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new PInterest();
        $form = $this->createForm(new PInterestType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('geo_pinterest_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PInterest entity.
     *
     * @Route("/{id}/edit", name="geo_pinterest_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:PInterest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PInterest entity.');
        }

        $editForm = $this->createForm(new PInterestType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing PInterest entity.
     *
     * @Route("/{id}/update", name="geo_pinterest_update")
     * @Method("POST")
     * @Template("GPSBundle:PInterest:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GPSBundle:PInterest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PInterest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PInterestType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('geo_pinterest_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a PInterest entity.
     *
     * @Route("/{id}/delete", name="geo_pinterest_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GPSBundle:PInterest')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PInterest entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('geo_pinterest'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
