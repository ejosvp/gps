<?php

namespace GPS\GPSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'label' => 'Filtro: ',
                'choices'   => array(
                    'trackId' => 'Por Placa',
                ),
            ))
            ->add('content', 'text', array(
                'label' => 'Dato: ',
            ))
            ->add('beginDate', 'date', array(
                'label' => 'Fecha de Inicio: ',
            ))
            ->add('beginTime', 'time', array(
                'label' => 'Hora de Inicio: ',
            ))
            ->add('endDate', 'date', array(
                'label' => 'Fecha de Termino: ',
            ))
            ->add('endTime', 'time', array(
                'label' => 'Hora de Termino: ',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        /**
         *
         */
    }

    public function getName()
    {
        return 'gps_gpsbundle_filtertype';
    }
}
