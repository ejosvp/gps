<?php

namespace GPS\GPSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PInterestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                "label" => "Nombre: "
            ))
            ->add('description', 'textarea', array(
                "label" => "Descripción: "
            ))
            ->add('address', 'text', array(
                "label" => "Dirección: "
            ))
            ->add('latitude', 'number', array(
                "label" => "Latitud: ",
                "precision" => 8
            ))
            ->add('longitude', 'number', array(
                "label" => "Longitud: ",
                "precision" => 8
            ))
            ->add('phone', 'text', array(
                "label" => "Teléfono: "
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\GPSBundle\Entity\PInterest'
        ));
    }

    public function getName()
    {
        return 'gps_gpsbundle_pinteresttype';
    }
}
