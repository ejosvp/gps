<?php

namespace GPS\GPSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VehicleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plate', 'text', array(
                'label' => 'Placa: ',
            ))
            ->add('driver', 'entity', array(
                'label' => 'Conductor: ',
                'class' => 'GPSBundle:Driver'
            ))
            ->add('brand', 'text', array(
                'label' => 'Marca: ',
            ))
            ->add('model', 'text', array(
                'label' => 'Modelo: ',
            ))
            ->add('year', 'number', array(
                'label' => 'Año: ',
            ))
            ->add('color', 'text', array(
                'label' => 'Color: ',
            ))
            ->add('insurance', 'text', array(
                'label' => 'Numero de seguro: ',
            ))
            ->add('insuranceStart_at', 'date', array(
                'label' => 'Fecha de inicio del seguro: ',
            ))
            ->add('insuranceEnd_at', 'date', array(
                'label' => 'Fecha de fin del seguro: ',
            ))
            ->add('insuranceCompany', 'text', array(
                'label' => 'Compañia de seguros: ',
            ))
            ->add('chip', 'text', array(
                'label' => 'Numero del chip: ',
            ))
            ->add('id', 'text', array(
                'label' => 'Id del GPS: ',
            ))
            ->add('installation_at', 'date', array(
                'label' => 'Fecha de instalación: ',
            ))
            ->add('trackerModel', 'text', array(
                'label' => 'Modelo del rastreador: ',
            ))
            ->add('trackerBrand', 'text', array(
                'label' => 'Marca del rastreador: ',
            ))
            ->add('group')
            ->add('file', 'file', array(
                'label' => 'Foto: ',
                'required' => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\GPSBundle\Entity\Vehicle'
        ));
    }

    public function getName()
    {
        return 'gps_gpsbundle_vehicletype';
    }
}
