<?php

namespace GPS\GPSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DriverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre: ',

            ))
            ->add('lastname', 'text', array(
                'label' => 'Apellidos: ',
            ))
            ->add('dni', 'text', array(
                'label' => 'Numero de DNI: ',
            ))
            ->add('bloodType', 'choice', array(
                'label' => 'Tipo de sangre: ',
                'choices'   => array(
                    'O-' => 'O-',
                    'O+' => 'O+',
                    'A-' => 'A-',
                    'A+' => 'A+',
                    'B-' => 'B-',
                    'B+' => 'B+',
                    'AB-' => 'AB-',
                    'AB+' => 'AB+',
                ),
            ))
            ->add('licenseCategory', 'text', array(
                'label' => 'Categoria de la licencia: ',
            ))
            ->add('license', 'text', array(
                'label' => 'Numero de licencia: ',
            ))
            ->add('licenseExpiration_at', 'date', array(
                'label' => 'Fecha de expiración de licencia: ',
            ))
            ->add('licenseRestriction', 'text', array(
                'label' => 'Restricción de la licencia: ',
            ))
            ->add('insurance', 'text', array(
                'label' => 'Numero de seguro: ',
            ))
            ->add('insuranceCompany', 'text', array(
                'label' => 'Compañia de seguro: ',
            ))
            ->add('insuranceExpiration_at', 'date', array(
                'label' => 'Fecha de expiración del seguro: ',
            ))
            ->add('file', 'file', array(
                'label' => 'Foto: ',
                'required' => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\GPSBundle\Entity\Driver'
        ));
    }

    public function getName()
    {
        return 'gps_gpsbundle_drivertype';
    }
}
