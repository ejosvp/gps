<?php

namespace GPS\GPSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GPS\GPSBundle\Entity\PlotRepository")
 * @ORM\Table(name="gps_data")
 */
class Plot
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Vehicle", inversedBy="plots")
     * @ORM\JoinColumn(name="trackId", referencedColumnName="id")
     */
    private $trackId;

    /**
     * @ORM\Column(type="time")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dataValidity;

    /**
     * @ORM\Column(type="decimal", scale=5)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", scale=5)
     */
    private $longitude;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $speedOverGround;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $trueCourse;

    /**
     * @ORM\Column(type="date")
     */
    private $datestamp;

    /**
     * @ORM\Column(type="decimal", scale=1, nullable=true)
     */
    private $magneticVariation;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $magneticVariationDirection;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $checksum;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $driver;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="array")
     */
    private $sensors;

    /**
     * Get id
     *
     * @ORM\return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setTrackId($trackId)
    {
        $this->trackId = $trackId;
    }

    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * Set timestamp
     *
     * @ORM\param \DateTime $timestamp
     * @ORM\return Data
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @ORM\return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set dataValidity
     *
     * @ORM\param boolean $dataValidity
     * @ORM\return Data
     */
    public function setDataValidity($dataValidity)
    {
        $this->dataValidity = $dataValidity;

        return $this;
    }

    /**
     * Get dataValidity
     *
     * @ORM\return boolean
     */
    public function getDataValidity()
    {
        return $this->dataValidity;
    }

    /**
     * Set latitude
     *
     * @ORM\param float $latitude
     * @ORM\return Data
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @ORM\return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @ORM\param float $longitude
     * @ORM\return Data
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @ORM\return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }


    /**
     * Set speedOverGround
     *
     * @ORM\param float $speedOverGround
     * @ORM\return Data
     */
    public function setSpeedOverGround($speedOverGround)
    {
        $this->speedOverGround = $speedOverGround;

        return $this;
    }

    /**
     * Get speedOverGround
     *
     * @ORM\return float
     */
    public function getSpeedOverGround()
    {
        return $this->speedOverGround;
    }

    /**
     * Set trueCourse
     *
     * @ORM\param float $trueCourse
     * @ORM\return Data
     */
    public function setTrueCourse($trueCourse)
    {
        $this->trueCourse = $trueCourse;

        return $this;
    }

    /**
     * Get trueCourse
     *
     * @ORM\return float
     */
    public function getTrueCourse()
    {
        return $this->trueCourse;
    }

    /**
     * Set datestamp
     *
     * @ORM\param \DateTime $datestamp
     * @ORM\return Data
     */
    public function setDatestamp($datestamp)
    {
        $this->datestamp = $datestamp;

        return $this;
    }

    /**
     * Get datestamp
     *
     * @ORM\return \DateTime
     */
    public function getDatestamp()
    {
        return $this->datestamp;
    }

    /**
     * Set magneticVariation
     *
     * @ORM\param float $magneticVariation
     * @ORM\return Data
     */
    public function setMagneticVariation($magneticVariation)
    {
        $this->magneticVariation = $magneticVariation;

        return $this;
    }

    /**
     * Get magneticVariation
     *
     * @ORM\return float
     */
    public function getMagneticVariation()
    {
        return $this->magneticVariation;
    }

    /**
     * Set magneticVariationDirection
     *
     * @ORM\param boolean $magneticVariationDirection
     * @ORM\return Data
     */
    public function setMagneticVariationDirection($magneticVariationDirection)
    {
        $this->magneticVariationDirection = $magneticVariationDirection;

        return $this;
    }

    /**
     * Get magneticVariationDirection
     *
     * @ORM\return boolean
     */
    public function getMagneticVariationDirection()
    {
        return $this->magneticVariationDirection;
    }

    /**
     * Set checksum
     *
     * @ORM\param string $checksum
     * @ORM\return Data
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Get checksum
     *
     * @ORM\return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    public function setSensors($sensors)
    {
        $this->sensors = $sensors;
    }

    public function getSensors()
    {
        return $this->sensors;
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }


}
