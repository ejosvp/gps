<?php

namespace GPS\GPSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GPS\GPSBundle\Model\AbstractPhoto;
use GPS\UserBundle\Entity\Group;

/**
 * Vehicle
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GPS\GPSBundle\Entity\VehicleRepository")
 */
class Vehicle extends AbstractPhoto
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plate", type="string", length=20)
     */
    private $plate;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Driver", inversedBy="vehicles")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=50)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=20)
     */
    private $model;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=20)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="insurance", type="string", length=50)
     */
    private $insurance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insuranceStart_at", type="date")
     */
    private $insuranceStart_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insuranceEnd_at", type="date")
     */
    private $insuranceEnd_at;

    /**
     * @var string
     *
     * @ORM\Column(name="insuranceCompany", type="string", length=50)
     */
    private $insuranceCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="chip", type="string", length=50)
     */
    private $chip;

    /**
     * @ORM\OneToMany(targetEntity="Plot", mappedBy="trackId")
     */
    private $plots;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="installation_at", type="date")
     */
    private $installation_at;

    /**
     * @var string
     *
     * @ORM\Column(name="trackerModel", type="string", length=20)
     */
    private $trackerModel;

    /**
     * @var string
     *
     * @ORM\Column(name="trackerBrand", type="string", length=20)
     */
    private $trackerBrand;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="GPS\UserBundle\Entity\Group", inversedBy="vehicles")
     */
    private $group;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set plate
     *
     * @param string $plate
     * @return Vehicle
     */
    public function setPlate($plate)
    {
        $this->plate = $plate;
    
        return $this;
    }

    /**
     * Get plate
     *
     * @return string 
     */
    public function getPlate()
    {
        return $this->plate;
    }

    /**
     * Set driver
     *
     * @param string $driver
     * @return Vehicle
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
    
        return $this;
    }

    /**
     * Get driver
     *
     * @return string 
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return Vehicle
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    
        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Vehicle
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Vehicle
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Vehicle
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set insurance
     *
     * @param string $insurance
     * @return Vehicle
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;
    
        return $this;
    }

    /**
     * Get insurance
     *
     * @return string 
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set insuranceStart_at
     *
     * @param \DateTime $insuranceStartAt
     * @return Vehicle
     */
    public function setInsuranceStartAt($insuranceStartAt)
    {
        $this->insuranceStart_at = $insuranceStartAt;
    
        return $this;
    }

    /**
     * Get insuranceStart_at
     *
     * @return \DateTime 
     */
    public function getInsuranceStartAt()
    {
        return $this->insuranceStart_at;
    }

    /**
     * Set insuranceEnd_at
     *
     * @param \DateTime $insuranceEndAt
     * @return Vehicle
     */
    public function setInsuranceEndAt($insuranceEndAt)
    {
        $this->insuranceEnd_at = $insuranceEndAt;
    
        return $this;
    }

    /**
     * Get insuranceEnd_at
     *
     * @return \DateTime 
     */
    public function getInsuranceEndAt()
    {
        return $this->insuranceEnd_at;
    }

    /**
     * Set insuranceCompany
     *
     * @param string $insuranceCompany
     * @return Vehicle
     */
    public function setInsuranceCompany($insuranceCompany)
    {
        $this->insuranceCompany = $insuranceCompany;
    
        return $this;
    }

    /**
     * Get insuranceCompany
     *
     * @return string 
     */
    public function getInsuranceCompany()
    {
        return $this->insuranceCompany;
    }

    /**
     * Set chip
     *
     * @param string $chip
     * @return Vehicle
     */
    public function setChip($chip)
    {
        $this->chip = $chip;
    
        return $this;
    }

    /**
     * Get chip
     *
     * @return string 
     */
    public function getChip()
    {
        return $this->chip;
    }

    /**
     * @param mixed $plots
     */
    public function setPlots($plots)
    {
        $this->plots = $plots;
    }

    /**
     * @return mixed
     */
    public function getPlots()
    {
        return $this->plots;
    }


    /**
     * Set installation_at
     *
     * @param \DateTime $installationAt
     * @return Vehicle
     */
    public function setInstallationAt($installationAt)
    {
        $this->installation_at = $installationAt;
    
        return $this;
    }

    /**
     * Get installation_at
     *
     * @return \DateTime 
     */
    public function getInstallationAt()
    {
        return $this->installation_at;
    }

    /**
     * @param string $trackerBrand
     */
    public function setTrackerBrand($trackerBrand)
    {
        $this->trackerBrand = $trackerBrand;
    }

    /**
     * @return string
     */
    public function getTrackerBrand()
    {
        return $this->trackerBrand;
    }

    /**
     * @param string $trackerModel
     */
    public function setTrackerModel($trackerModel)
    {
        $this->trackerModel = $trackerModel;
    }

    /**
     * @return string
     */
    public function getTrackerModel()
    {
        return $this->trackerModel;
    }

    protected function getUploadDir()
    {
        return 'uploads/vehicle';
    }

    /**
     * @param \GPS\GPSBundle\Entity\boolena $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param \GPS\UserBundle\Entity\Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return \GPS\UserBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return \GPS\GPSBundle\Entity\boolena
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function isEnabled()
    {
        return $this->getStatus();
    }


}
