<?php

namespace GPS\GPSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GPS\GPSBundle\Model\AbstractPhoto;

/**
 * Driver
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GPS\GPSBundle\Entity\DriverRepository")
 */
class Driver extends AbstractPhoto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=10)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="bloodType", type="string", length=6)
     */
    private $bloodType;

    /**
     * @var string
     *
     * @ORM\Column(name="licenseCategory", type="string", length=10)
     */
    private $licenseCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="license", type="string", length=25)
     */
    private $license;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="licenseExpiration_at", type="datetime")
     */
    private $licenseExpiration_at;

    /**
     * @var string
     *
     * @ORM\Column(name="licenseRestriction", type="string", length=255)
     */
    private $licenseRestriction;

    /**
     * @var string
     *
     * @ORM\Column(name="insurance", type="string", length=50)
     */
    private $insurance;

    /**
     * @var string
     *
     * @ORM\Column(name="insuranceCompany", type="string", length=60)
     */
    private $insuranceCompany;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insuranceExpiration_at", type="datetime")
     */
    private $insuranceExpiration_at;

    /**
     * @var Object
     *
     * @ORM\OneToMany(targetEntity="Vehicle", mappedBy="driver")
     */
    private $vehicles;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = 0;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function isEnabled()
    {
        return $this->getStatus();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Driver
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Driver
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Driver
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    
        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set bloodType
     *
     * @param string $bloodType
     * @return Driver
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;
    
        return $this;
    }

    /**
     * Get bloodType
     *
     * @return string 
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * Set licenseCategory
     *
     * @param string $licenseCategory
     * @return Driver
     */
    public function setLicenseCategory($licenseCategory)
    {
        $this->licenseCategory = $licenseCategory;
    
        return $this;
    }

    /**
     * Get licenseCategory
     *
     * @return string 
     */
    public function getLicenseCategory()
    {
        return $this->licenseCategory;
    }

    /**
     * Set license
     *
     * @param string $license
     * @return Driver
     */
    public function setLicense($license)
    {
        $this->license = $license;
    
        return $this;
    }

    /**
     * Get license
     *
     * @return string 
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set licenseExpiration_at
     *
     * @param \DateTime $licenseExpirationAt
     * @return Driver
     */
    public function setLicenseExpirationAt($licenseExpirationAt)
    {
        $this->licenseExpiration_at = $licenseExpirationAt;
    
        return $this;
    }

    /**
     * Get licenseExpiration_at
     *
     * @return \DateTime 
     */
    public function getLicenseExpirationAt()
    {
        return $this->licenseExpiration_at;
    }

    /**
     * Set licenseRestriction
     *
     * @param string $licenseRestriction
     * @return Driver
     */
    public function setLicenseRestriction($licenseRestriction)
    {
        $this->licenseRestriction = $licenseRestriction;
    
        return $this;
    }

    /**
     * Get licenseRestriction
     *
     * @return string 
     */
    public function getLicenseRestriction()
    {
        return $this->licenseRestriction;
    }

    /**
     * Set insurance
     *
     * @param string $insurance
     * @return Driver
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;
    
        return $this;
    }

    /**
     * Get insurance
     *
     * @return string 
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set insuranceCompany
     *
     * @param string $insuranceCompany
     * @return Driver
     */
    public function setInsuranceCompany($insuranceCompany)
    {
        $this->insuranceCompany = $insuranceCompany;
    
        return $this;
    }

    /**
     * Get insuranceCompany
     *
     * @return string 
     */
    public function getInsuranceCompany()
    {
        return $this->insuranceCompany;
    }

    /**
     * Set insuranceExpiration_at
     *
     * @param \DateTime $insuranceExpirationAt
     * @return Driver
     */
    public function setInsuranceExpirationAt($insuranceExpirationAt)
    {
        $this->insuranceExpiration_at = $insuranceExpirationAt;
    
        return $this;
    }

    /**
     * Get insuranceExpiration_at
     *
     * @return \DateTime 
     */
    public function getInsuranceExpirationAt()
    {
        return $this->insuranceExpiration_at;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Driver
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __toString(){
        return $this->getName() . ' ' . $this->getLastname();
    }

    /**
     * @param Object $vehicles
     */
    public function setVehicles($vehicles)
    {
        $this->vehicles = $vehicles;
    }

    /**
     * @return Object
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }


    protected function getUploadDir()
    {
        return 'uploads/drivers';
    }
}
